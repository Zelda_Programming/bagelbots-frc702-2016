package org.usfirst.frc.team702.robot;

import edu.wpi.first.wpilibj.Timer;

public class DebouncedBoolean {

	protected boolean m_previousSample;
	protected boolean m_outputSample;
	
	protected int m_samples;
	protected int m_trueCount;
	
	protected boolean m_isstable;
	
	protected double m_timeUpdate;
	protected static final double m_setTime = 0.1;
	
	public DebouncedBoolean(int samples) {
		
		this.setSamples(samples);
		this.m_trueCount = 0;
		this.m_timeUpdate = getTime();
		this.m_isstable = false;
	}
	
	
	/**
	 * Call this method each time the value is updated
	 * 
	 * @param sample - the boolean value of the sampled input
	 * @return the debounced output
	 */
	public synchronized void update(boolean sample)
	{
		if(sample != m_previousSample)
		{
			// If a change was detected, set the timestamp
			m_timeUpdate = getTime();
			this.m_isstable = false;
			
		} else if(sample == m_previousSample && getTime() - m_timeUpdate >= m_setTime){
			// otherwise, if the sample has been the same for more than the set time, change the output
			this.set(sample);
		}
		
		m_previousSample = sample;
		
		//return this.get();
	}
	
	protected synchronized void set(boolean value)
	{
		this.m_outputSample = value;
		this.m_isstable = true;
	}
	
	public synchronized boolean get()
	{
		return m_outputSample;
	}
	
	/**
	 * @param samples - number of samples needed for output to be considered true
	 */
	public synchronized void setSamples(int samples)
	{
		this.m_samples = samples;
	}
	
	protected double getTime()
	{
		double timeVal = 0.0;
		try {
			timeVal = Timer.getFPGATimestamp();
		} catch (Exception e) {
			timeVal = System.currentTimeMillis();
			//e.printStackTrace();
		}
		
		return timeVal;
	}

}
