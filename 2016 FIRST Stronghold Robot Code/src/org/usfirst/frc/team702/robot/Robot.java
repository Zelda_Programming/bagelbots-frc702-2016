/**
 * This code was written over the course of 6 weeks for the FIRST Stronghold competition for 2016. This was written by the programming department of
 * Team 702, "The Bagel Bytes" which consisted of the members Ezra "Zelda" Reese and Maki "Weeaboo" and with the help of mentors Scott "Azules" and Sabri
 * "Rocket". This program is open-source to anyone who is interested in using/looking at the code. If you have any questions, feel free to email Ezra 
 * at Zelda@planet702.org.
 */

package org.usfirst.frc.team702.robot;

import com.kauailabs.navx.frc.AHRS;

import java.util.LinkedList;
import java.util.List;

import com.ctre.CANTalon;

import org.usfirst.frc.team702.robot.Shooter.ShootState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import org.usfirst.frc.team702.robot.RateLimiter;

/**
 * 
 * @authors Ezra "Zelda", Scott "Azules", Maki "Weeaboo", Sabri "Rocket"
 *
 */

public class Robot extends SampleRobot {
	// LIDAR
	AnalogInput LIDAR;
	
	// Ultrasonic Sensor
	AnalogInput ultrasonic;
	public double m_ultrasonic_slope_correction = 41.22449;
	public double m_ultrasonic_bias = -4.6559;
			
	// Practice / Final Robot Switch
	public static final boolean practiceMode = false;
	
	// Power Distribution Board
	PowerDistributionPanel powerBoard;
	
	// Drive Train Motors (CANBus)
	CANTalon motor0;
	CANTalon motor1;
	CANTalon motor2;
	CANTalon motor3;
	
	// Drive Train Motors (PMW)
	Talon nMotor0;
	Talon nMotor1;
	Talon nMotor2;
	Talon nMotor3;
	
	// Arm Motors (CANBus & PWM)
	CANTalon armBaneBot24;
	Talon nArmBaneBot5;

	// Collector Motor (CANBus & PWM)
	CANTalon collectMotor25;
	Talon nCollectMotor4;
	
	// Shooter Mechanism (CANBus & PWM)
	//CANTalon shooterMech26;
	//Talon nShooterMech7;
	
	// Shooter Flywheels (CANBus & PWM)
	CANTalon flyWheel27;
	CANTalon flyWheel28;
	Talon nFlyWheel2;
	Talon nFlyWheel3;
	
	// Placeholder Talon (CANBus & PWM)
	CANTalon placeHolder;
	Talon nPlaceHolder;
	
	// Relays (Multi-Use)
	Relay shooterPin;
	
	// Controller Declarations (Multi-Use)
	Joystick driverCont;
	Joystick coDriverCont;
	
	// RobotDrives (CANBus)
	RobotDrive bagelTank;
	RobotDrive coDriverArmDrive;
	RobotDrive coShooterMech;
	RAWDrive RAWDrive;
	
	// RobotDrives (PWM)
	RobotDrive nBagelTank;
	RobotDrive nCoDriverArmDrive;
	RobotDrive nCoShooterMech;
	RAWDrive nRAWDrive;
	
	// Camera Stuff
	CameraServer camServ;
	//Image capture;
	
	// navx libraries
	AHRS navx;
	SerialPort serial_port;
		
	// I2C
    LidarLite_V1 lidarSensor;
		
	// Pixy Camera
	PixyCmu5 pixyCamera;
	List<PixyCmu5.PixyFrame> pixyFrame;
	
	// Other
	Timer shootTimer;
	
	// Autonomous Selector
	SendableChooser autoChooser;
	AutonomousModes selectedAutonomous;
	
	// Smart Dashboard preferences
	Preferences prefs;
	
	public enum AutonomousModes{
		NONE,
		DRIVE_STRAIGHT,
		DRIVE_LOWBAR,
		DRIVE_SHOOT
	}
			
	// Doubles
	double lsyA;
	double rsyA;
	double zTrigL;
	double zTrigR;
	double zTrigTot;
	double cozTrig;
	double colsyA;
	double corsyA;
	double preShootTimeStamp;
	double degFromCenter = 0;
	double gamma = 2.0;
	
	// CoDriver Buttons
	boolean buttonA;
	boolean buttonB;
	boolean buttonX;
	boolean buttonY;
	boolean buttonLB;
	boolean buttonRB;
	boolean buttonBack;
	boolean buttonStart;
	
	// Driver Buttons
	boolean buttonA2;
	boolean buttonB2;
	//boolean buttonX2; - remove possibly forever
	OneShotBoolean db_ButtonX2;
	OneShotBoolean db_ButtonY2;
	OneShotDouble  db_DriverPOV;
	
	OneShotBoolean db_buttonRB;
	OneShotBoolean db_buttonLB;
	
	// Other Booleans
	boolean shooterPreShot = true;
	boolean i2c_tf = false;
	boolean autoNorth = true;
	boolean autoSouth = false;
	
	// Floats
	
	// Bytes
	byte dataBuffer;
	byte current_in_Hertz = 50; //for navx    
    
	public Robot() {
		
    // Motor Assignment (Final Robot)
	if(!practiceMode)
	{
	    motor0 = new CANTalon(20);
	    motor1 = new CANTalon(21);
	    motor2 = new CANTalon(22);
	    motor3 = new CANTalon(23);
	    armBaneBot24 = new CANTalon(24);
	    collectMotor25 = new CANTalon(25);
	    //shooterMech26 = new CANTalon(29);
	    flyWheel27 = new CANTalon(27);
	    flyWheel28 = new CANTalon(28);
	    placeHolder = new CANTalon(80);
	    
	    // Drivetrain Assignment (Final Robot)
	    //bagelTank = new RobotDrive(motor2, motor3, motor0, motor1);
	    RAWDrive = new RAWDrive (motor2, motor3, motor0, motor1);
	    coDriverArmDrive = new RobotDrive(armBaneBot24, placeHolder);
	    //coShooterMech = new RobotDrive (shooterMech26, placeHolder);
	}

	db_ButtonX2 = new OneShotBoolean(10);
	db_ButtonY2 = new OneShotBoolean(10);
	db_DriverPOV = new OneShotDouble(-1,.1);
	
	db_buttonRB = new OneShotBoolean(10);
	db_buttonLB = new OneShotBoolean(10);
	
	ultrasonic = new AnalogInput(1);
	
	if(practiceMode)
	{
	    // Motor Assignment (Practice Robot)
	    nMotor0 = new Talon(0);
	    nMotor1 = new Talon(1);
	    nFlyWheel2 = new Talon(2);
	    nFlyWheel3 = new Talon(3);
	    nCollectMotor4 = new Talon(4);
	    nArmBaneBot5 = new Talon(5);
	    //nShooterMech7 = new Talon(7);
	    nPlaceHolder = new Talon(10);
	
	    // DriveTrain Assignment (Practice Robot)
	    //nBagelTank = new RobotDrive (nMotor0, nMotor0, nMotor1, nMotor1);
	    nRAWDrive = new RAWDrive (nMotor0, nMotor0, nMotor1, nMotor1);
	    nRAWDrive.setImu(navx);
	    
	    nCoDriverArmDrive = new RobotDrive(nArmBaneBot5, nPlaceHolder);
	   // nCoShooterMech = new RobotDrive (nShooterMech7, nPlaceHolder);
	}
	
    //Relay Assignment (Final Robot & Practice Robot)
    shooterPin = new Relay(0);

    // Joystick Assignment (Final & Practice Robots)
    driverCont = new Joystick(0);
    coDriverCont = new Joystick(1);
    
    // Power Distribution Board
    powerBoard = new PowerDistributionPanel();
        
    // To Get Camera Visual Stream to the FRC Dashboard [AT ALL TIMES (RAW IMAGE)]
    //camServ = CameraServer.getInstance();
    //camServ.setQuality(50);
    //camServ.startAutomaticCapture("cam0");  
    
    //Motor Inversion for Drive Train (final robot)
    if(!practiceMode)
	{
	    RAWDrive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, false);
	    RAWDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, false);
	    RAWDrive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, false);
	    RAWDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, false);
	}
    
    //Motor Inversion for Drive Train (practice robot)
    if(practiceMode)
	{
	    nRAWDrive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
	    nRAWDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
	    nRAWDrive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
	    nRAWDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
	}
    
    prefs = Preferences.getInstance();
    
//    SmartDashboard.putData("ZeroYaw", new Command() {
//		public void initialize() { navx.zeroYaw(); }
//		public void interrupted() { }
//		public void end() { }
//		public void execute() { }
//		public boolean isFinished() { return true; }
//	});
    
    }
/**
 * The following loop is where the Pixy camera we use for vision tracking and the LIDAR we use for distance reading is instantiated. This loop is ran 
 * on the first initialization of the robot when it turns on. This is to reduce memory usage and prevent any form of memory leak from these two devices
 * as they are crucial to our success in the competition.     
 */
	
    public void robotInit() { 	 	
    // Allocate memory for the Pixy Frame Data
    	try
    	{
    		// Pixy Port Assignment (Final & Practice Robots)
    		
    		/*
    		 * Instantiate a new Pixy object at address 168 and schedule it to read data
    		 * at a 1 second period. This data will be accessable by calling pixyCamera.getCurrentframes()
    		 */
    		pixyFrame = new LinkedList<PixyCmu5.PixyFrame>();
    	    pixyCamera = new PixyCmu5(168, .25);
    		
    	} catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating Pixy:  " + ex.getMessage(), true);
	    }
    	
    	try
    	{
    		/*
    		 * Instantiate a new LidarLite_v1 object at I2C address 0x62 and schedule it to read
    		 * data at a .5 second period. This data will be accessable by calling
    		 * 		lidarSensor.getDistance()
			 * 		lidarSensor.getDataTimestamp()
			 * 		lidarSensor.getDataAge()
			 * 		lidarSensor.isDataValid()
			 * 
			 * To asyncronously access data, call lidarSensor.getData()
    		 */
    		lidarSensor = new LidarLite_V1(0x62);
    		
    	} catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating LidarLite V1:  " + ex.getMessage(), true);
	    }
	   	
	   	// navx Port Assignment (Final & Practice Robots)
	    try {
	        /* Communicate w/navX MXP via the MXP SPI Bus.                                     */
	        /* Alternatively:  I2C.Port.kMXP, SerialPort.Port.kMXP or SerialPort.Port.kUSB     */
	        /* See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface/ for details. */
	    	navx = new AHRS(SPI.Port.kMXP, current_in_Hertz);
	    	navx.reset();
	    	RAWDrive.setImu(navx);
	    	
	    } catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating navX MXP:  " + ex.getMessage(), true);
	    }
	    
	    /**
	     * Smart Dashboard initialization
	     */
	    
	    // Autonomous selection
	    autoChooser = new SendableChooser();
	    SmartDashboard.putString("Selected Autonomous Mode","unknown");
	    autoChooser.addDefault("No Autonomous", AutonomousModes.NONE);
	    autoChooser.addObject("Drive Straight", AutonomousModes.DRIVE_STRAIGHT);
	    autoChooser.addObject("Drive Shoot", AutonomousModes.DRIVE_SHOOT);
	    autoChooser.addObject("Lowbar", AutonomousModes.DRIVE_LOWBAR);
	    SmartDashboard.putData("Autonomous Mode Chooser",autoChooser);

    }
    
    /**
     * Disabled should go here. Users should overload this method to run code that
     * should run while the field is disabled.
     *
     * Called once each time the robot enters the disabled state.
     */
    protected void disabled() {
      System.out.println("Default disabled() method running, consider providing your own");
      
      while(isDisabled())
      {
	      selectedAutonomous = (AutonomousModes) autoChooser.getSelected();
	      SmartDashboard.putString("Selected Autonomous Mode",selectedAutonomous.toString());
	      
	      Timer.delay(0.2);
      }
    }
    
    
/**
 * The autonomous loop is where all automated code is written and executed from. Autonomous is a 15 second piece of the overall competition where, at the
 * start of the match, the robot must perform an automated task (like driving forward) in order to score additional points. Autonomous is by far one of
 * the most difficult pieces of the game due to its short time and its difficult (while looking simple) tasks the robot must perform.
 * For this challenge we must travel 6ft 2in.
 */
    
    
    public void autonomous() {
    	navx.reset();
    	
    double timeAutonomousStart = 0;
    boolean firstTime = true;	
   
    RateLimiter rateLimit = new RateLimiter();
    rateLimit.initMovingAverageLimiter(10);
    
    selectedAutonomous = (AutonomousModes) autoChooser.getSelected();
    SmartDashboard.putString("Selected Autonomous Mode",selectedAutonomous.toString());
    
    for(int idx = 0; idx < 10; idx++)
    {
    	rateLimit.limit(0);
    }
    
    if(navx != null && RAWDrive != null)
    {
    	RAWDrive.setImu(navx);
    }
    
    while (isAutonomous() && isEnabled()){
    	float yaw = navx.getYaw();
    	float xDisplacement = navx.getDisplacementX();

    	// Set up time for different autonomous modes
    	final int armDownTimeStart = 0;
    	final int armDownTimeDuration = 2;
    	
    	final int timeDriveStart = 1;
    	final int timeDriveDuration = 4;
    	
    	final int totalArmDuration = armDownTimeStart + armDownTimeDuration;
    	final int totalDriveDuration = timeDriveStart + timeDriveDuration;
    			
    	if(firstTime)
    	{
    		navx.resetDisplacement();
    		navx.reset();
    		timeAutonomousStart = Timer.getFPGATimestamp();
    		firstTime = false;
    	}

    	switch(selectedAutonomous)
    	{
    		case DRIVE_STRAIGHT:
    			
    			// Drive for 4 seconds going forward
        		if(		(Timer.getFPGATimestamp() - timeAutonomousStart > timeDriveStart) && 
        				(Timer.getFPGATimestamp() - timeAutonomousStart < (timeDriveStart + timeDriveDuration)))
        		{
        			RAWDrive.drive(-0.7, (yaw/(gamma*180)));
        			showOnDash(9, (yaw/(gamma*180)));
        		} else {
        			RAWDrive.drive(0, 0);
        		}
    			
    			break;
    		case DRIVE_LOWBAR:
    			
    			// Lower Arm
    			if( (Timer.getFPGATimestamp() - timeAutonomousStart > armDownTimeStart) && 
					(Timer.getFPGATimestamp() - timeAutonomousStart < (armDownTimeStart + armDownTimeDuration)))
    			{
    				armBaneBot24.set(-0.65);
    				
    			// Drive Forward
    			} else if((Timer.getFPGATimestamp() - timeAutonomousStart > totalArmDuration) && 
    					 (Timer.getFPGATimestamp() - timeAutonomousStart < (timeDriveDuration + totalArmDuration)))
    			{
    				// Turn off the banebot
    				armBaneBot24.set(0);
    				
    				// TODO: rateLimit code: rateLimit.limit(-0.7)
    				RAWDrive.drive(-0.7, (yaw/(gamma*180)));
    				showOnDash(9, (yaw/(gamma*180)));
        		} else {
        			armBaneBot24.set(0);
        			RAWDrive.drive(0, 0);
        		}
    			
    			break;
    			
    		default: // NO AUTONOMOUS MODE
    			
    	}

    }
    }
    
/**
 * Voids are loops that allow you to perform certain functions from anywhere in the code. These voids are used for flywheel control. More can be made
 * if needed. Generally, voids are used to perform multiple functions at one time in one line.
 */
    
	    // Shooter Void (final robot)
	    void flyWheelShoot()
	    {
	    	flyWheel27.set(1.0);
	    	flyWheel28.set(-1.0);
	    }
	    
	    void flyWheelReverse()
	    {
	    	flyWheel27.set(-0.7);
	    	flyWheel28.set(0.7);
	    }
	    
	    void flyWheelStop()
	    {
	    	flyWheel27.set(0.0);
	    	flyWheel28.set(0.0);
	    }
    
    //Shooter Void (practice robot)
    void nFlyWheelShoot()
    {
    	nFlyWheel2.set(0.7);
    	nFlyWheel3.set(-0.7);
    }
    
    void nFlyWheelReverse()
    {
    	nFlyWheel2.set(-0.7);
    	nFlyWheel3.set(0.7);
    }
    
    void nFlyWheelStop()
    {
    	nFlyWheel2.set(0.0);
    	nFlyWheel3.set(0.0);
    }
    
/**
 * Operator Control is the teleoperated period of the competition where robots are controled by human drivers. In this loop we have the shooting mechanism,
 * the drive code, the collection code, the assignment of buttons and joysticks to various variables declared at the top of the code and numerous other
 * functions. This is the most developed part of the code.
 */
    
    public void operatorControl() {
    	// navx clearing
    	navx.zeroYaw();
    	navx.resetDisplacement();
    	
    	// Declarations of various variables only to be used locally in Operator Control
    	String test = ""; 
    	boolean on = true;
    	boolean off = false;
    	boolean shootForward = true;
    	boolean shootReverse = false;
    	boolean m_autoTarget = false;
    	double m_previousPixyTime = 0;
    	
    	double loopTimer;
    	double maxLoopTimer = 0;
    	
    	// Shooting Booleans
    	boolean shootStartup = true;
    	boolean shootInit = false;
    	boolean shootPhase1 = false;
    	boolean shootPhase2 = false;
    	
    	String pixyTargetDetected = "target NOT detected";
    	
    	double robotRange_feet = 0;
    	
    	/**
    	 * Shooter state machine variables
    	 */
    	
    	ShootState s_currentShootState = ShootState.STANDBY;
		ShootState s_nextShootState = ShootState.STANDBY;
		Timer s_timeInState = new Timer();
		int   s_countsInState = 0;
		
    	// System initialization (final robot)
    	String startUp = "Starting Final Operator Control";
    	//showOnDash(9, startUp);
    	
    	// System initialization (practice robot)
    	String nStartUp = "Starting Practice Operator Control";
    	//showOnDash(9, nStartUp);
    	
    	// Speed limiters for the arm and shooting mechanisms to reduce potential damage. (final and practice robots)
		if (!practiceMode)
		{
			coDriverArmDrive.setMaxOutput(0.8);
		}

		if (practiceMode)
		{
			nCoDriverArmDrive.setMaxOutput(0.7);
		}
		
    while (isOperatorControl() && isEnabled()) {
    	loopTimer = Timer.getFPGATimestamp();
    	// Double Assignments for Controllers (Axis')
    	rsyA = driverCont.getRawAxis(1);
    	lsyA = driverCont.getRawAxis(5);
    	colsyA = coDriverCont.getRawAxis(1);
    	corsyA = coDriverCont.getRawAxis(5);
    	zTrigL = driverCont.getRawAxis(2);
    	zTrigR = driverCont.getRawAxis(3);
    	// add a deadband to the Z axis trigger
    	zTrigL = (zTrigL < 0.2 ? 0 : -zTrigL); // backwards
    	zTrigR = (zTrigR < 0.2 ? 0 : zTrigR); // forwards
  
    	zTrigTot = -1*(zTrigR + zTrigL);
    	
    	double USV = ultrasonic.getVoltage();
    	// Ultrasonic sensor calculation
    	robotRange_feet = (USV * m_ultrasonic_slope_correction + m_ultrasonic_bias)/12.0;
 
    	// Haptic Feedback Code for Controllers
    	float rawAccelZ = navx.getWorldLinearAccelZ();
    	float zAccel = Math.abs(Math.min(Math.max(Math.abs(rawAccelZ) < 0.5 ? 0 : rawAccelZ, -1), 1)/1);
    	driverCont.setRumble(Joystick.RumbleType.kLeftRumble, zAccel/2);
    	driverCont.setRumble(Joystick.RumbleType.kRightRumble, zAccel/2);
    	   	
    	// Boolean Assignments for Controller (Buttons)
    	buttonA = coDriverCont.getRawButton(1);
    	buttonA2 = driverCont.getRawButton(1);
    	buttonB = coDriverCont.getRawButton(2);
    	buttonB2 = driverCont.getRawButton(2);
    	buttonX = coDriverCont.getRawButton(3);
    	buttonY = coDriverCont.getRawButton(4);
    	buttonLB = coDriverCont.getRawButton(5);
    	buttonRB = coDriverCont.getRawButton(6);
    	buttonBack = coDriverCont.getRawButton(7);
    	buttonStart = coDriverCont.getRawButton(8);
    	
    	// Update the debounced buttons
    	db_ButtonY2.update(driverCont.getRawButton(4)); //buttonY2 = driverCont.getRawButton(4);
    	db_ButtonX2.update(driverCont.getRawButton(3)); // removed buttonX2 = 
    	db_DriverPOV.update(driverCont.getPOV());
    	db_buttonRB.update(driverCont.getRawButton(6));
    	db_buttonLB.update(driverCont.getRawButton(5));
    	
    	float latchedPOV = (float)db_DriverPOV.get();
    	
    	if (!practiceMode)
    	{
    		if (latchedPOV != -1 && !RAWDrive.isAutoEnable())
    		{
    			RAWDrive.turnToAngle(latchedPOV, true);
    			RAWDrive.setAutoEnable(true);
    		}
    	}
    	
    	// Snap-to-Angel Mechanism (practice robot)
    	if (practiceMode)
    	{
    		if (latchedPOV != -1 && !nRAWDrive.isAutoEnable())
    		{
    			nRAWDrive.turnToAngle(latchedPOV, true);
    			nRAWDrive.setAutoEnable(true);
    			//System.out.println("Turning to Angle: " + Float.toString(latchedPOV));
    		}
    	}
    	
    	/**
    	 * 
    	 **/
    	
    	// Shooting Mechanism (deadband made using ternary "if") (final robot)
//    	if (!practiceMode)
//		{
//	    	coShooterMech.arcadeDrive(corsyA > -0.2 && corsyA < 0.2 ? 0 : corsyA, 0);
//	    }
//    	
//    	// Shooting Mechanism (deadband made using ternary "if") (practice robot)
//		if (practiceMode)
//		{
//			nCoShooterMech.arcadeDrive(corsyA > -0.2 && corsyA < 0.2 ? 0 : corsyA, 0);
//		}
    	
    	// Bane Bot Arm Mechanism (deadband made using ternary "if") (final robot)
		if (!practiceMode)
		{
			coDriverArmDrive.arcadeDrive(colsyA > -0.2 && colsyA < 0.2 ? 0 : -colsyA, 0);
		}
    	
    	// Bane Bot Arm Mechanism (deadband made using ternary "if") (practice robot)
		if (practiceMode)
		{
			nCoDriverArmDrive.arcadeDrive(colsyA > -0.2 && colsyA < 0.2 ? 0 : -colsyA, 0);
	    }
		
    	// Shooter Mechanism (final robot)
		if (practiceMode)
		{
			
	    	if(buttonLB && !buttonRB && !buttonBack)
	    	{
	    	nFlyWheelShoot();
	    		shootReverse = true;
	    		shootForward = false;
	    	}else if(buttonBack && !buttonLB && !buttonRB)
	    	{
	    		nFlyWheelStop();
	    		shootForward = true;
	    		shootReverse = false;
	    	}
	    	
		}
		
		/**
		 * DEBUG CODE
		 * TODO: remove
		 */
		// If an object is detected in the frame
		if(!pixyCamera.getCurrentframes().isEmpty())
		{
			pixyTargetDetected = "TARGET DETECTED";
			
			try
			{
			// Calculate the number of degrees from the center the current frame 
			degFromCenter = PixyCmu5.degreesXFromCenter(pixyCamera.getCurrentframes().get(0));
			showOnDash(2, Double.toString(degFromCenter) + " degrees from target");
			
			} catch  (RuntimeException ex ){

			}
		} else {
			pixyTargetDetected = "target NOT detected";
			showOnDash(2, "no target");
		}
		
		showOnDash(0, s_currentShootState.toString());
		maxLoopTimer = Math.max(maxLoopTimer, Timer.getFPGATimestamp() - loopTimer);
    	showOnDash(1, "Range: " + Double.toString(Math.floor(10*robotRange_feet)/10) + " Feet");
    	showOnDash(3, pixyTargetDetected);
    	showOnDash(4, "Z Trigers: " + Double.toString(zTrigTot));
    	//System.out.println(USV);
    	//showOnDash(4, "Voltage: " + Volts. + " Volts");
    	
		if(RAWDrive != null && RAWDrive.navX != null)
		{
		/**
		 * TODO: Use for PID Loop Debugging
		 */
		//showOnDash(4, (RAWDrive.isAutoEnable()) ? 1 : 0);
		//showOnDash(5, RAWDrive.getAngleSetpoint());
		//showOnDash(6, RAWDrive.anglePIDLoop.getError());
		}
		
		/**
		 * Implimentation of the auto targetting system. Uses PixyCmu5 and the command pixyCamera.isDetectedAndCentered()
		 * to activate the automatic functions.
		 **/		

		if(!practiceMode)
		{
			
			switch(s_currentShootState)
			{
			
				case STANDBY:
					// If the number of times we are in this state 
					if(s_countsInState == 0)
					{
						System.out.println(" Robot::Shooter - Entering Standby State!");
						s_timeInState.start();
					}
					
					s_countsInState++;
					
					// Wait on some event to indicate the shoot cycle should start
					if(buttonRB)
					{
						s_nextShootState = ShootState.STARTUP;
					}
					
					break;
					
				case STARTUP:
					// If the number of times we are in this state 
					if(s_countsInState == 0)
					{
						System.out.println(" Robot::Shooter - STARTUP - Entering Startup State!");
						s_timeInState.start();
					}
					s_countsInState++;
					
					if(pixyCamera.isObjectDetected())
					{
						s_nextShootState = ShootState.LOCK;
						System.out.println(" Robot::Shooter - Startup - Object Detected!");
					} else if(s_timeInState.get() > Shooter.max_time_STARTUP){ // If we have been waiting for the pixy acquisition for > 3 secs
						s_nextShootState = ShootState.SHUTDOWN;
					}
					
					break;
					
				case LOCK:
					if(s_countsInState == 0)
					{
						System.out.println(" Robot::Shooter - Lock - Acquiring Target Lock!");
						s_timeInState.start();
					}
					s_countsInState++;
					
					// If an object is detected in the frame
					if(!pixyCamera.getCurrentframes().isEmpty())
					{
						// Calculate the number of degrees from the center the current frame 
						degFromCenter = PixyCmu5.degreesXFromCenter(pixyCamera.getCurrentframes().get(0));
						
						if(Math.abs(degFromCenter) > 5*PixyCmu5.PIXY_X_DEG_PER_PIXEL)
						{
							// Command the robot to turn to the angle offset from the current rotation
							RAWDrive.turnToAngle((float)(-1*degFromCenter), true);
						} else {
							// We are on target, spin the flywheels and shoot
							System.out.println(" Robot::Shooter - Lock - TARGET LOCKED!");
							s_nextShootState = ShootState.FLYWHEEL;
						}
					} else { // No frames are detected
						s_nextShootState = ShootState.SHUTDOWN;
						System.out.println(" Robot::Shooter - Lock - No objects detected, cancelling target lock!");
					}
					
					
					// If we have been trying to lock for too long, shutdown and return control
					if(s_timeInState.get() > Shooter.max_time_LOCK){
						s_nextShootState = ShootState.SHUTDOWN;
						//s_nextShootState = ShootState.FLYWHEEL; // TODO: COMMENT THIS OUT FOR REAL CODE
						System.out.println(" Robot::Shooter - Lock - Target lock timed out. Shutting down!");
					}
					
					break;
					
				case FLYWHEEL:
					// If the number of times we are in this state 
					if(s_countsInState == 0)
					{
						System.out.println("Robot::Shooter - Flywheel - Entering Flywheel State!");
						s_timeInState.start();
					}
					s_countsInState++;
					
					// Spin up the flywheels
					flyWheelShoot();
					
					if(s_timeInState.get() > Shooter.max_time_FLYWHEEEL)
					{
						System.out.println("Robot::Shooter - Flywheel - Flywheels Spinning!");
						s_nextShootState = ShootState.COLLECTOR;
					}
					
					break;
					
				case COLLECTOR:
					// If the number of times we are in this state 
					if(s_countsInState == 0)
					{
						System.out.println("Robot::Shooter - Collector - Entering Collector State!");
						s_timeInState.start();
					}
					s_countsInState++;
					
					flyWheelShoot();
					collectMotor25.set(1.0);
					
					if(s_timeInState.get() > Shooter.max_time_COLLECTOR)
					{
						s_nextShootState = ShootState.SHUTDOWN;
						System.out.println("Robot::Shooter - Collector - Shutting Down!");
					}
					
					break;
					
				case SHUTDOWN:
					// If the number of times we are in this state 
					if(s_countsInState == 0)
					{
						System.out.println("Robot::Shooter - Shutdown - Entering Shutdown State!");
						s_timeInState.start();
					}
					s_countsInState++;
					
					flyWheelStop();
					collectMotor25.set(0.0);
					
					if(s_timeInState.get() > Shooter.max_time_SHUTDOWN)
					{
						s_nextShootState = ShootState.STANDBY;
					}
					
					break;
				default:
					break;
			}
			
			if(s_currentShootState != ShootState.STANDBY && areSticksMoving(lsyA, rsyA))
			{
				s_nextShootState = ShootState.SHUTDOWN;
			}
			
			// A state change is detected
			if(s_currentShootState != s_nextShootState)
			{
				s_timeInState.reset();
				s_timeInState.stop();
				s_countsInState = 0;
				
				// Push us into the next state for the next iteration of the Operator while loop
				s_currentShootState = s_nextShootState;
			}
			
		}
		
    	// Shooter Mechanism (practice robot hard shot)
		if (!practiceMode)
		{	
			
			if(buttonX && !buttonY)
	    	{
	    		flyWheelShoot();
	    	}
			
			if(buttonY && !buttonX)
	    	{
	    		flyWheelStop();
	    	}
			
		} 
		
    	// Shooting Pin Mechanism (keeps shooter in "shooting" configuration)
    	/*
    	if(buttonX && on && !off) 
    	{
    		shooterPin.set(Relay.Value.kForward);
    		on = false;
    		off = true;
    	}else if(buttonX && off && !on)
    	{
    		shooterPin.set(Relay.Value.kReverse);
    		on = true;
    		off = false;
    	}else
    	{
    		shooterPin.set(Relay.Value.kOff);
    	}
    	*/
    	
    	// Collecting Mechanism (final robot)
		if(s_currentShootState == ShootState.STANDBY)
		{
			if (!practiceMode)
			{
				if(buttonA && !buttonB)
				{
					collectMotor25.set(1.0);
				}else if(buttonB && !buttonA)
				{
					collectMotor25.set(-1.0);
				}else if (!buttonA && !buttonB)
				{	
					collectMotor25.set(0.0);
				}
			}
		}
    	
    	// Collecting Mechanism (practice robot)
		if (practiceMode)
		{
	    	if(buttonA && !buttonB)
	    	{
	    		nCollectMotor4.set(1.0);
	    	}else if(buttonB && !buttonA)
	    	{
	    		nCollectMotor4.set(-1.0);
	    	}
		}
    	
    	// I2C Testing (Using NetConsole) (these buttons are on Driver control (slot 0 in drive station)
    	if(buttonA2) // Trigger reading data from the Lidar on NetConsole (should give you a number, that's how far away the object in question is
    				 // in centimeters (will change to meters on Monday)
    	{
    		System.out.println("Most Recent Lidar Data: " +
    							Double.toString(lidarSensor.getDistance()) + "cm. " +
    							"Detected " + Double.toString(lidarSensor.getDataAge()) + " seconds ago."
    						   );
    		
    		System.out.println("Performing Lidar Read...");
    		System.out.println("New Reading: " + Double.toString(lidarSensor.getData()) + "cm.");
    		Timer.delay(0.2);
    	}
    	
    	
    	if(buttonB2) // Trigger reading data from the Pixy on NetConsole (look for xcenter value)
    	{
    		//Empty the pixyFrame list if it has any data
    		pixyFrame.clear();
    		pixyFrame = pixyCamera.getFrames();
    		Timer.delay(0.2);
    	}
    	    	
    	// Increase Proportional Gain
    	/*
    	if(db_buttonRB.get())
    	{
    		RAWDrive.setkP(nRAWDrive.getkP()+0.001);
    	}
    	*/
    	
    	// Decrease Proportional Gain
    	/*
    	if(db_buttonLB.get())
    	{
    		RAWDrive.setkP(nRAWDrive.getkP()-0.001);
    	}
    	*/
    	
    	/*
    	if(db_ButtonY2.get())  // Will tell you if Lidar is wired correctly and detected on roborio
    	{
    		RAWDrive.setImu(navx);
    		//pixyCamera.stop();
    		//lidarSensor.stop();
    		maxLoopTimer = 0;
    	}
    	*/
    	
    	// naxX ZeroYaw
    	if(db_ButtonX2.get())
    	{
    		m_autoTarget = !m_autoTarget;
    	}
    	
    	/*
    	if(!this.pixyCamera.getCurrentframes().isEmpty())
    	{

    		if(m_autoTarget && m_previousPixyTime < this.pixyCamera.getCurrentframes().get(0).timestamp && this.pixyCamera.getDataAge() < 1)
    		{
    			// Calculate the number of degrees from the center the current frame 
    			degFromCenter = PixyCmu5.degreesXFromCenter(pixyCamera.getCurrentframes().get(0));
    			System.out.println("Entering Turn To Pixy!");
    			if(Math.abs(degFromCenter) > 5)
    			{
    				System.out.println("Turn To " + Double.toString(degFromCenter));
    				// Command the robot to turn to the angle offset from the current rotation
    				RAWDrive.turnToAngle((float)(1*degFromCenter), true);
    			} else {
    				System.out.println("NO Turn To " + Double.toString(degFromCenter));
    			}
    		}

    		m_previousPixyTime = this.pixyCamera.getCurrentframes().get(0).timestamp;
    	}
    	*/
    		
    		
    		//nRAWDrive.turnToAngle(5, true);
			//nRAWDrive.setAutoEnable(true);
    		//navx.reset();
    		//pixyCamera.start(1.0);
    		//lidarSensor.start(0.5);
    	
    	
    	// TankDrive (drive train) code (final robot)
    	if (!practiceMode)
		{
    		//bagelTank.tankDrive(rsyA, lsyA);
    		if(areSticksMoving(lsyA, rsyA))
    		{
    			RAWDrive.tankDrive(lsyA, rsyA);
    		} else {
    			RAWDrive.driveStraight(zTrigTot);
    		}
		}
    	
    	// TankDrive (drive train) code (practice robot)
    	if (practiceMode)
		{
    		//nBagelTank.tankDrive(rsyA, lsyA);
    		nRAWDrive.tankDrive(rsyA, lsyA);
		}
    	
    	}
    	// Disable rumble when not enabled
    	driverCont.setRumble(Joystick.RumbleType.kLeftRumble, 0);
    	driverCont.setRumble(Joystick.RumbleType.kRightRumble, 0);
    }

    
    
/**
 * This is the test loop. This allows you to test the robot code before deployment. This sends back data to the smartdashboard test tab 
 * so we can see what the output of our code is. It is empty because we use the Eclipse debugging system.    
 **/
    
    public void test() {
    	LiveWindow.setEnabled(false);
    
    	while(isTest() && isEnabled())
    	{
	    	if(RAWDrive.anglePIDLoop != null)
	    	{
	    		RAWDrive.updateSmartDashboardData();
	    		Timer.delay(5);
	    		RAWDrive.getSmartDashboardData();
	    	}
	    	
	    	Timer.delay(0.01);
    	}
    }
    
/**
 * These last private voids are for the smartdashboard. This allows us to send any data to the dashboard that we may wish to display for the coDriver.
 **/
    
    private void showOnDash(int slot, String value) {
    	String dbString = String.format("DB/String %d", slot);
    	SmartDashboard.putString(dbString, value);
    }
    
    // Convenience type overloads.
    private void showOnDash(int slot, int value) { showOnDash(slot, Integer.toString(value)); }
    private void showOnDash(int slot, long value) { showOnDash(slot, Long.toString(value)); }
    private void showOnDash(int slot, float value) { showOnDash(slot, Float.toString(value)); }
    private void showOnDash(int slot, double value) { showOnDash(slot, Double.toString(value)); }

    
    public static boolean areSticksMoving(double leftStick, double rightStick)
    {
    	return !(Math.abs(leftStick) < 0.3 && Math.abs(rightStick) < 0.3);
    }
}